package ru.t1.strelcov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.comparator.*;

import java.util.Comparator;

public enum SortType {

    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", ComparatorByDateStart.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    public static boolean isValidByName(@Nullable final String name) {
        for (final SortType sortType : values()) {
            if (sortType.toString().equals(name))
                return true;
        }
        return false;
    }

    SortType(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

}
