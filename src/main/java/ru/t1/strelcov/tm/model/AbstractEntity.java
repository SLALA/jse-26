package ru.t1.strelcov.tm.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
public abstract class AbstractEntity {

    @NotNull
    protected final String id = UUID.randomUUID().toString();

}
